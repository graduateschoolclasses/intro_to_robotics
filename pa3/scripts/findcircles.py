#!/usr/bin/env python
import cv2
import cv2.cv as cv
from sensor_msgs.msg import LaserScan
import numpy as np
import rospy, message_filters
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Pose, Point
from nav_msgs.msg import Odometry
import math
from tf.transformations import euler_from_quaternion, quaternion_from_euler

class circleFinder:
	def __init__(self):
		self.sub1 = message_filters.Subscriber('/scan', LaserScan)
		self.sub2 = message_filters.Subscriber('odom', Odometry)
		self.ts = message_filters.ApproximateTimeSynchronizer([self.sub1,self.sub2], 10, 1)
		self.ts.registerCallback(self.callback)
		self.pub = rospy.Publisher('visualization_marker_array', MarkerArray, queue_size = 10)
		self.height = 10.0
		self.width = 10.0
		self.resolution = .02
		self.initx = 2.0
		self.inity = 2.0

		# self.pub_explore = rospy.Publisher('/exploration_polygon_marker', MarkerArray, queue_size = 10)
		# self.pub = rospy.Publisher('visualization_marker_array', Marker, queue_size = 10)

		# points = []
		#
		# point_1 = Point()
		# point_1.x = 0
		# point_1.y = 0
		# point_1.z = 0
		#
		# points.append(point_1)
		#
		# point_2 = Point()
		# point_1.x = 0
		# point_1.y = 10
		# point_1.z = 0
		#
		# points.append(point_2)
		#
		# point_3 = Point()
		# point_1.x = 10
		# point_1.y = 10
		# point_1.z = 0
		#
		# points.append(point_3)
		#
		# point_4 = Point()
		# point_1.x = 10
		# point_1.y = 0
		# point_1.z = 0
		#
		# points.append(point_4)
		#
		# explore_marker = Marker()
		# explore_marker.header.frame_id = "map"
		# explore_marker.ns = "mynamespace"
		# explore_marker.type = 4
		# explore_marker.scale.x = .2
		# explore_marker.color.a = 1.0
		# explore_marker.color.r = 1.0
		# explore_marker.points = points
		# self.pub_explore.publish(explore_marker)


	def callback(self, scan, odom):
		markerArray = MarkerArray()
		self.image = self.generateImage(scan, odom)
		# lines = self.findLines()
		circles = self.findCircles()
		counter = 0
		resolution = self.resolution
		height = self.height/resolution
		# if circles != None:
		# 	for num,obj in enumerate(circles):
		# 		print ######################
		# 		print num
		# 		print obj
		# 		print obj[0]
		# 		print ######################

			# print circles

		if circles != None:
			for circle in circles:
				x = float(circle[0])
				y = float(circle[1])
				r = float(circle[2])
				# print (x,y,r)
				marker = Marker()
				marker.header.frame_id = "map"
				marker.ns = "mynamespace"
				marker.id = counter
				marker.type = 3
				marker.action = marker.ADD
				marker.scale.x = 1
				marker.scale.y = 1
				marker.scale.z = 0.1
				marker.color.a = 1.0
				marker.color.g = 1.0
				marker.pose = self.generatePose(x,y,resolution)
				markerArray.markers.append(marker)
				# self.pub.publish(marker)
				counter = counter+1
		self.pub.publish(markerArray)

	def generatePose(self, x,y, res):
		xsize = self.width
		ysize = self.height
		resolution = self.resolution
		height = int(ysize/resolution)
		width = int(xsize/resolution)
		p = Pose()
		p.position.x = x * res
		p.position.y = (height-y) * res
		# print "###################################"
		# print "x"
		# print x
		# print p.position.x
		# print width
		# print "###################################"
		# print "y"
		# print y
		# print p.position.y
		# print height
		# print "###################################"

		# p.position.x = 0
		# p.position.y = 0

		# p.position.z = 0.0
		# ro = 0.0
		# pi = 0.0
		# h = math.sqrt((x2 - x1)*(x2 - x1)+(y2 - y1)*(y2 - y1))
		# ya = math.acos((x2-x1)/h)
		# quat = quaternion_from_euler(ro, pi, ya)
		# p.orientation.x = quat[0]
		# p.orientation.y = quat[1]
		# p.orientation.z = quat[2]
		# p.orientation.w = quat[3]
		return p

	def generateImage(self, scan, odom):
		xsize = self.width
		ysize = self.height
		resolution = self.resolution
		height = int(ysize/resolution)

		width = int(xsize/resolution)
		blank_image = np.zeros((height,width,3), np.uint8)
		xraw = odom.pose.pose.position.x + self.initx
		x0 = int(xraw/resolution)
		yraw = odom.pose.pose.position.y + self.inity
		y0 = int(yraw/resolution)
		quaternion = (
			odom.pose.pose.orientation.x,
			odom.pose.pose.orientation.y,
			odom.pose.pose.orientation.z,
			odom.pose.pose.orientation.w)
		euler = euler_from_quaternion(quaternion)
		yaw = euler[2]

		maxAngle = scan.angle_max
		minAngle = scan.angle_min
		angleInc = scan.angle_increment
		maxLength = scan.range_max
		distances = scan.ranges
		numScans = len(distances)

		for i in range(numScans):
			angle = yaw + minAngle + float(i)*angleInc
			x = x0 + (distances[i]*math.cos(angle)/resolution)
			y = y0 + int(distances[i]*math.sin(angle)/resolution)
			y = height - y
			try:
				blank_image[y,x] = [255,255,255]
			except:
				pass
		blank_image[height - y0,x0] = [0,255,0]
		# cv2.imshow('blank_image', blank_image), cv2.waitKey(3)
		return blank_image

	def findLines(self):
		gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
		edges = cv2.Canny(gray, 50, 150, apertureSize = 3)
		# This section of code was adapted from opencv python tutorials
		lines = cv2.HoughLinesP(edges,1,np.pi/180,20,1,25)
		if lines != None:
			for x1,y1,x2,y2 in lines[0]:
				cv2.line(self.image,(x1,y1),(x2,y2),(0,255,0),2)
		cv2.imshow('result', self.image), cv2.waitKey(3)
		return lines

	def findCircles(self):
		# gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
		# edges = cv2.Canny(gray, 50, 150, apertureSize = 3)
		# # This section of code was adapted from opencv python tutorials
		# lines = cv2.HoughLinesP(edges,1,np.pi/180,20,1,25)
		# if lines != None:
		# 	for x1,y1,x2,y2 in lines[0]:
		# 		cv2.line(self.image,(x1,y1),(x2,y2),(0,255,0),2)
		# cv2.imshow('result', self.image), cv2.waitKey(3)

		gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
		edges = cv2.Canny(gray, 50, 150, apertureSize = 3)
		circles = cv2.HoughCircles(edges, cv2.cv.CV_HOUGH_GRADIENT,1,20,param1=80,param2=10,minRadius=15,maxRadius=30)
		# circles = cv2.HoughCircles(edges, cv2.cv.CV_HOUGH_GRADIENT,2,20,param1=75,param2=20,minRadius=18,maxRadius=25)
		# print circles
		# ensure at least some circles were found
		if circles is not None:
		# convert the (x, y) coordinates and radius of the circles to integers
			circles = np.round(circles[0, :]).astype("int")

			# loop over the (x, y) coordinates and radius of the circles
			for (x, y, r) in circles:
				# draw the circle in the output image, then draw a rectangle
				# corresponding to the center of the circle
				cv2.circle(self.image, (x, y), r, (0, 255, 0), 4)
				# cv2.rectangle(self.image, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)

		cv2.imshow('result', self.image), cv2.waitKey(3)
		return circles


if __name__=="__main__":
	rospy.init_node('markers')
	circles = circleFinder()
	rospy.spin()

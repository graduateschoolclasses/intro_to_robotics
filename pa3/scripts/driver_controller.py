#!/usr/bin/env python
import roslib
roslib.load_manifest('pa3')
import rospy
from geometry_msgs.msg import Twist
from geometry_msgs.msg import TwistStamped
from sensor_msgs.msg import LaserScan
import message_filters

class controller:
	def __init__(self, distance):
		rospy.loginfo('Start Init')
		self.distance = distance
		self.teleop_sub = message_filters.Subscriber('/twist_stamped',TwistStamped)
		self.scan_sub = message_filters.Subscriber('/scan',LaserScan)
		self.ts = message_filters.TimeSynchronizer([self.teleop_sub,self.scan_sub], 10)
		self.ts.registerCallback(self.stopper_callback)
		self.twist_command_pub = rospy.Publisher('/cmd_vel_mux/input/teleop', Twist, queue_size=3)
		self.command = Twist()
		rospy.loginfo('End Init')

	def stopper_callback(self, teleop_twist_stamped, laserscan):
		laserscan.ranges
		laserscan_len = (len(laserscan.ranges))
		array_split = laserscan_len/2
		array_split_split = array_split/2
		array_split_split_spit = array_split_split/3
		min_index = array_split-array_split_split-(2*array_split_split_spit)
		max_index = array_split+array_split_split+(2*array_split_split_spit)
		modified_array = laserscan.ranges[min_index:max_index]
		closest = min(modified_array)
		if (closest <= self.distance):
			if teleop_twist_stamped.twist.linear.x >= 0:
				self.command.linear.x = 0.0
				self.command.linear.y = 0.0
				self.command.linear.z = 0.0
				self.command.angular.x = 0.0
				self.command.angular.y = 0.0
				self.command.angular.z = 0.0
				self.twist_command_pub.publish(self.command)
				rospy.loginfo('Stopped %s from obstacle' % self.distance)
			else:
				self.twist_command_pub.publish(teleop_twist_stamped.twist)
				rospy.loginfo('Reversing')
		else:
			self.twist_command_pub.publish(teleop_twist_stamped.twist)

if __name__ == '__main__':
	rospy.init_node('stopper',log_level=rospy.DEBUG)
	distance = rospy.get_param('driver_controller/distance', .5)
	controller = controller(distance)
	rospy.spin()

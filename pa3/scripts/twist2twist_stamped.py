#!/usr/bin/env python

#This node converts from twist to twiststamped messages this is useful for
#syncing teleop commands with other topics such as sensors.
#Author: Benjamin Narin

import roslib
roslib.load_manifest('pa3')
import rospy
from geometry_msgs.msg import Twist
from geometry_msgs.msg import TwistStamped
from std_msgs.msg import Header
import time

class controller:
	def __init__(self):

		print "start Init"
		self.twist_sub = rospy.Subscriber("/key_vel",Twist,self.twist_callback)
		self.twist_stamped_pub = rospy.Publisher('/twist_stamped', TwistStamped, queue_size=3)
		self.twist_stamped = TwistStamped()
		self.header = Header()
		print "End Init"

	def twist_callback(self, twist_message):
		self.twist_stamped.twist = twist_message
		self.header.stamp = rospy.Time.now()
		self.twist_stamped.header = self.header
		self.twist_stamped_pub.publish(self.twist_stamped)

if __name__ == '__main__':
	rospy.init_node('twist2twist_stamped',log_level=rospy.DEBUG)
	controller = controller()
	rospy.spin()

#!/usr/bin/env python
import rospy
from smach import StateMachine
from smach_ros import SimpleActionState
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import csv
from visualization_msgs.msg import Marker
waypoint_list = []

# waypoints = [['one', (2.1, 2.2), (0.0, 0.0, 0.0, 1.0)],['two', (6.5, 4.43), (0.0, 0.0, -0.984047240305, 0.177907360295)]]


if __name__ == '__main__':
	rospy.init_node('patrol')
	waypoint_viz_pub = rospy.Publisher('waypoint_viz', Marker, queue_size=10)
	waypoint_marker = Marker()
	with open('output.csv', 'rb') as f:
		reader = csv.reader(f)
		for row in reader:
			waypoint_list.append(row)
		print waypoint_list
	for w in waypoint_list:
		waypoint_marker.header.frame_id = w[1]
		waypoint_marker.header.stamp = rospy.get_rostime()
		waypoint_marker.id = 0
		waypoint_marker.type = 2
		waypoint_marker.action = 0
		waypoint_marker.pose.position.x = float(w[2])
		waypoint_marker.pose.position.y = float(w[3])
		waypoint_marker.pose.position.z = float(w[4])
		waypoint_marker.pose.orientation.x = float(w[5])
		waypoint_marker.pose.orientation.y = float(w[6])
		waypoint_marker.pose.orientation.z = float(w[7])
		waypoint_marker.pose.orientation.w = float(w[8])
		waypoint_marker.scale.x = 1.0
		waypoint_marker.scale.y = 1.0
		waypoint_marker.scale.z = 1.0
		waypoint_marker.color.r = 1.0
		waypoint_marker.color.g = 0.0
		waypoint_marker.color.b = 0.0
		waypoint_marker.color.a = 0.5
		waypoint_marker.lifetime = rospy.Duration(0)
		print waypoint_marker
		waypoint_viz_pub.publish(waypoint_marker)
		print "Marker Placed"


	patrol = StateMachine(['succeeded','aborted','preempted'])
	with patrol:
		for i,w in enumerate(waypoint_list):
			goal_pose = MoveBaseGoal()
			goal_pose.target_pose.header.frame_id = w[1]
			goal_pose.target_pose.pose.position.x = float(w[2])
			goal_pose.target_pose.pose.position.y = float(w[3])
			goal_pose.target_pose.pose.position.z = float(w[4])
			goal_pose.target_pose.pose.orientation.x = float(w[5])
			goal_pose.target_pose.pose.orientation.y = float(w[6])
			goal_pose.target_pose.pose.orientation.z = float(w[7])
			goal_pose.target_pose.pose.orientation.w = float(w[8])
			print goal_pose


			StateMachine.add(w[0], SimpleActionState('move_base',MoveBaseAction,goal=goal_pose),transitions={'succeeded':waypoint_list[(i + 1) % len(waypoint_list)][0]})
	patrol.execute()

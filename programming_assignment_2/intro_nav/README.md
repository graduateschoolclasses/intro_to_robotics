Running Instructions
Path Creator (rosrun must be run in scripts folder of intro_nav):
roslaunch path_creator.launch
rosrun intro_nav path_creator.py

Using Rviz place nav goals, these will be saved out to a CSV when the node is exited (Ctl-c)

Running Instructions
Path Creator (rosrun must be run in scripts folder of intro_nav):
roslaunch intro_nav nav.launch
rosrun intro_nav path_follower.py

Keyboard controls will open in their own window, these can be used to randomize the waypoints or back to the first waypoint
When done exiting the nav code (ctl-c) will save out all the sensor data

CSV Output Files:
output.csv contains the nav goals created by running path_creator.py

sensor_readings.csv contains the minimal sensor reading at each of the waypoints

If unable to find maps:
export TURTLEBOT_STAGE_WORLD_FILE="/opt/ros/indigo/share/turtlebot_stage/maps/stage/maze.world"
export TURTLEBOT_STAGE_MAP_FILE="/opt/ros/indigo/share/turtlebot_stage/maps/maze.yaml"

Author: Benjamin Narin
Code Assignment 1: Introduction To Robotics

Python Nodes
driver_controller.py: Part 1 of the assignment, drives forward and stops .5m from an obstacle.
stopper_controller.py: Part 2 & 3 of the assignment, allows the user to drive and stops from hitting obstacle.
twist2twist_stamped.py: Node to convert from twist nessages to twist stamped messages to allow for sync with sensor topic.

Launch Files
driver.launch: Part 1 of the assignment.
stopper.launch: Part 2 & 3 of the assignment, teleop node spawns in new window. 

#!/usr/bin/env python
import roslib
roslib.load_manifest('exercise1')
import rospy
from geometry_msgs.msg import Twist 
from sensor_msgs.msg import LaserScan

class controller:
	def __init__(self, distance):
		rospy.loginfo('Start Init')
		self.distance = distance
		self.scan_sub = rospy.Subscriber('/scan',LaserScan,self.scan_callback)
		self.twist_command_pub = rospy.Publisher('/cmd_vel_mux/input/teleop', Twist, queue_size=3)
		self.command = Twist()
		rospy.loginfo('End Init')

	def scan_callback(self,laserscan):
		laserscan.ranges
		# closest = min(laserscan.ranges)
		laserscan_len = (len(laserscan.ranges))
		array_split = laserscan_len/2
		array_split_split = array_split/2
		array_split_split_spit = array_split_split/2
		min_index = array_split-array_split_split-array_split_split_spit
		max_index = array_split+array_split_split+array_split_split_spit
		modified_array = laserscan.ranges[min_index:max_index]
		closest = min(modified_array)
		if (closest <= self.distance):
			self.command.linear.x = 0.0
			self.command.linear.y = 0.0
			self.command.linear.z = 0.0
			self.command.angular.x = 0.0
			self.command.angular.y = 0.0
			self.command.angular.z = 0.0
			self.twist_command_pub.publish(self.command)
			rospy.loginfo('Stopped %s from obstacle' % self.distance)
		else:
			self.command.linear.x = 1.0
			self.command.linear.y = 0.0
			self.command.linear.z = 0.0
			self.command.angular.x = 0.0
			self.command.angular.y = 0.0
			self.command.angular.z = 0.0
			self.twist_command_pub.publish(self.command)
			rospy.loginfo('Driving Forward')

if __name__ == '__main__':
	rospy.init_node('stopper',log_level=rospy.DEBUG)
	distance = rospy.get_param('stopper/distance', .5)
	controller = controller(distance)
	rospy.spin()

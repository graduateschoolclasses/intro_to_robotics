#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
mu, sigma = 0, 0.1

m = 1
# x = t
b = 0

t = np.arange(-5.0, 5.0, 0.01)

s = np.piecewise(t, [t < 0, (t < 2) & (t>=0)], [lambda x: 0*x, lambda x: (m*x+b),lambda x: 2])
# s = np.piecewise(t, [t<=.5,.5<t<1,t>=1],[-1,m*t+b,1])

for index,value in enumerate(s):
	s[index] = value + np.random.normal(mu, sigma, 1)

print s
plt.plot(t, s)

plt.xlabel('x')
plt.ylabel('y')
plt.title('Sine Function')
plt.grid(True)
# plt.savefig("sine.png")
plt.show()

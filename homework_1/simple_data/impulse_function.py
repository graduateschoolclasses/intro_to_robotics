#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np

x = np.arange(0.0, 2.0, 0.01)
a = np.zeros( (len(x),) )   # whatever size. initializes to zeros
a[150:155] = 1.0          # index range sets location, width of impulse

print a

plt.step(x, a)
plt.show()

#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np

m = 4
# x = t
b = 0

t = np.arange(0.0, 2.0, 0.01) #Range of 0-2 with points at .01
s = (m*t+b)
print s
plt.plot(t, s)

plt.xlabel('x')
plt.ylabel('y')
plt.title('Linear Function')
plt.grid(True)
# plt.savefig("line.png")
plt.show()

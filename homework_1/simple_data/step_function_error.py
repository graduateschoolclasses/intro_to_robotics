#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
mu, sigma = 0, 0.1

x = np.arange(0.0, 2.0, 0.01)
y = (0.5 * (np.sign(x-.5) + 1))

for index,value in enumerate(y):
	y[index] = value + np.random.normal(mu, sigma, 1)

print y

plt.step(x, y)
plt.show()

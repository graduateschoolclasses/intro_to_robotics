#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np

mu, sigma = 0, 0.1
m = 4
# x = t
b = 0

t = np.arange(0.0, 2.0, 0.01) #Range of 0-2 with points at .01
s = (m*t+b)
for index,value in enumerate(s):
	s[index] = value + np.random.normal(mu, sigma, 1)

print s
plt.plot(t, s)

plt.xlabel('x')
plt.ylabel('y')
plt.title('Linear Function')
plt.grid(True)
# plt.savefig("line_error.png")
plt.show()

#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np

x = np.arange(0.0, 2.0, 0.01)
y = (0.5 * (np.sign(x-.5) + 1))

print y

plt.step(x, y)
plt.show()

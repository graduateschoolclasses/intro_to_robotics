#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
mu, sigma = 0, 0.1

t = np.arange(0.0, 2.0, 0.01)
# s = (np.sin(2*np.pi*t)+np.random.normal(mu, sigma, 1))
s = (np.sin(2*np.pi*t))
for index,value in enumerate(s):
    s[index] = value + np.random.normal(mu, sigma, 1)
print s
plt.plot(t, s)

plt.xlabel('x')
plt.ylabel('y')
plt.title('Sine Function')
plt.grid(True)
# plt.savefig("sine_error.png")
plt.show()

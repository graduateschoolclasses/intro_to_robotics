#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(0.0, 2.0, 0.01)
s = np.sin(2*np.pi*t)
print s
plt.plot(t, s)

plt.xlabel('x')
plt.ylabel('y')
plt.title('Sine Function')
plt.grid(True)
# plt.savefig("sine.png")
plt.show()

#!/usr/bin/env python
import cv2.cv as cv
import math

pi = math.pi
rho=.5 #Distance resolution of the accumulator in pixels
theta=pi/180 #Angle resolution of the accumulator in radians
threshold = 170 #Accumulator threshold parameter. Only those lines are returned that get enough votes ( >\texttt{threshold} ).

img = cv.LoadImage('building.jpg', 0) #Load Image
dst = cv.CreateImage(cv.GetSize(img), 8, 1) #Create Dummy Image
cv.Canny(img, dst, 200, 200) # Edge Dectctiion with Canny Saved to dst
cv.Threshold(dst, dst, 100, 255, cv.CV_THRESH_BINARY) #Create Binary Image
color_dst_standard = cv.CreateImage(cv.GetSize(img), 8, 3) #Create Dummy Image
cv.CvtColor(img, color_dst_standard, cv.CV_GRAY2BGR) #Greyscale img and save to color_dst_standard

lines = cv.HoughLines2(dst, cv.CreateMemStorage(0), cv.CV_HOUGH_STANDARD, rho, theta, threshold, 0, 0) #hough_transform
# lines = cv.HoughLines2(dst, cv.CreateMemStorage(0), cv.CV_HOUGH_STANDARD, rho, theta, threshold, 0, 0) #hough_transform
for (rho, theta) in lines[:100]: #Loop Over Lines Drawing Them On color_dst_standard
	a = math.cos(theta) # Calculate orientation
	b = math.sin(theta)
	x0 = a * rho
	y0 = b * rho
	pt1 = (cv.Round(x0 + 1000*(-b)), cv.Round(y0 + 1000*(a))) #Point 1
	pt2 = (cv.Round(x0 - 1000*(-b)), cv.Round(y0 - 1000*(a))) #Point 2
	cv.Line(color_dst_standard, pt1, pt2, cv.CV_RGB(255, 0, 0), 2, 4) #Draw the line

cv.ShowImage('Original Image',img)
cv.ShowImage("Canny Edge Detection", dst)
cv.ShowImage("Hough Transform", color_dst_standard)
cv.WaitKey(0)

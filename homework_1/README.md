Data Functions Folder:
	Final python files to generate the data, add noise and run filters
	gen.sh runs all the python scripts
filter_node Folder:
	ROS node for filtering, rivz config included
	running:roslaunch filter_node filter.launch
hough_transform Folder
	contains code to run hough transform
	running: ./hough_lines
	Two example jpgs included for testing
Simple Data:
	Broken up data functions (written as practice/experiments)

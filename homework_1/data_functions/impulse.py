#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import copy
mu, sigma = 0, 0.1

window_size = 20

def fuzz_it(array):
	fuzzed_array = copy.deepcopy(array)
	for index,value in enumerate(fuzzed_array):
		fuzzed_array[index] = value + np.random.normal(mu, sigma, 1)
	return fuzzed_array

def mean_filter(window,array):
	copy_array = copy.deepcopy(array)
	mean_array = np.zeros((len(copy_array),))
	for i,value in enumerate(copy_array):
		q = ((i+1) - window)
		if (q < 0):
			slice_array = copy_array[0:i]
		else:
			slice_array = copy_array[(i-window):i]
		mean_array[i] = np.mean(slice_array)
	return mean_array

def median_filter(window,array):
	copy_array = copy.deepcopy(array)
	mean_array = np.zeros((len(copy_array),))
	for i,value in enumerate(copy_array):
		q = ((i+1) - window)
		if (q < 0):
			slice_array = copy_array[0:i]
		else:
			slice_array = copy_array[(i-window):i]
		mean_array[i] = np.median(slice_array)
	return mean_array


t = np.arange(-5.0, 5.0, 0.01)
a = np.zeros( (len(t),) )   # whatever size. initializes to zeros
a[150:155] = 3.0          # index range sets location, width of impulse

fuzzy_array = fuzz_it(a)

mean_array = mean_filter(window_size,fuzzy_array)
median_array = median_filter(window_size,fuzzy_array)
#
# print a
# print "#########################################3"
# print fuzzy_array
# print "#########################################3"
# print mean_array

# plt.xlabel('x')
# plt.ylabel('y')
# plt.title('Impulse')
# plt.step(t, a)
# plt.show()
#
# plt.xlabel('x')
# plt.ylabel('y')
# plt.title('Impulse With Noise')
# plt.step(t, fuzzy_array)
# plt.show()
#
# plt.xlabel('x')
# plt.ylabel('y')
# plt.title('Impulse With Noise & Mean Filter')
# plt.step(t, mean_array)
# plt.show()
#
# plt.xlabel('x')
# plt.ylabel('y')
# plt.title('Impulse With Noise & Median Filter')
# plt.step(t, median_array)
# plt.show()

fig = plt.figure()

ax1 = fig.add_subplot(221)
ax1.set_title('Impulse')
ax1.step(t, a)

ax2 = fig.add_subplot(222)
ax2.set_title('Impulse With Noise')
ax2.step(t, fuzzy_array)

ax3 = fig.add_subplot(223)
ax3.set_title('Mean Filter')
ax3.step(t, mean_array)

ax4 = fig.add_subplot(224)
ax4.set_title('Median Filter')
ax4.step(t, median_array)

plt.savefig("Impulse.png")
fig.show()

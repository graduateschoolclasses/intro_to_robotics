#!/usr/bin/env python
import roslib
roslib.load_manifest('filter_node')
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
import copy
import numpy as np
import time
import datetime
import std_msgs.msg

class filters:
	def __init__(self):
		rospy.loginfo('Start Init')
		self.mean_scan_sub = rospy.Subscriber('/scan',LaserScan,self.mean_scan_callback)
		self.median_scan_sub = rospy.Subscriber('/scan',LaserScan,self.median_scan_callback)
		self.mean_filter_pub = rospy.Publisher('/scan/mean_filter', LaserScan, queue_size=3)
		self.median_filter_pub = rospy.Publisher('/scan/median_filter', LaserScan, queue_size=3)
		self.mean_message = LaserScan()
		self.median_message = LaserScan()
		self.window_size = 5
		rospy.loginfo('End Init')

	def mean_filter(self,window,array):
		copy_array = copy.deepcopy(array)
		mean_array = np.zeros((len(copy_array),))
		for i,value in enumerate(copy_array):
			q = ((i+1) - window)
			if (i == 0):
				slice_array = copy_array[i]
			elif (q < 0):
				slice_array = copy_array[0:i]
			else:
				slice_array = copy_array[(i-window):i]
			mean_array[i] = np.mean(slice_array)
		return mean_array

	def median_filter(self,window,array):
		copy_array = copy.deepcopy(array)
		median_array = np.zeros((len(copy_array),))
		for i,value in enumerate(copy_array):
			q = ((i+1) - window)
			if (i == 0):
				slice_array = copy_array[i]
			elif (q < 0):
				slice_array = copy_array[0:i]
			else:
				slice_array = copy_array[(i-window):i]
			median_array[i] = np.median(slice_array)
		return median_array

	def mean_scan_callback(self,laserscan):
		scan = laserscan.ranges
		window = self.window_size
		mean_values = filters.mean_filter(window,scan)
		in_array = np.zeros((len(mean_values),))
		for i,x in enumerate (in_array):
			in_array[i] = 1.0

		# laserscan.header.stamp = rospy.Time.now()
		# laserscan.ranges = mean_values
		# laserscan.range_min = min(mean_values)
		# laserscan.range_max = max(mean_values)


		self.mean_message.header.stamp = rospy.Time.now()
		self.mean_message.header.frame_id = laserscan.header.frame_id
		self.mean_message.angle_min = laserscan.angle_min
		self.mean_message.angle_max = laserscan.angle_max
		self.mean_message.angle_increment = laserscan.angle_increment
		self.mean_message.scan_time = laserscan.scan_time
		self.mean_message.ranges = mean_values
		self.mean_message.range_min = min(mean_values)
		self.mean_message.range_max = max(mean_values)
		self.mean_message.intensities = in_array
		self.mean_filter_pub.publish(self.mean_message)


	def median_scan_callback(self,laserscan):
		scan = laserscan.ranges
		window = self.window_size
		median_values = filters.median_filter(window,scan)
		in_array = np.zeros((len(median_values),))
		for i,x in enumerate (in_array):
			in_array[i] = 1.0
		self.median_message.header.stamp = rospy.Time.now()
		self.median_message.header.frame_id = laserscan.header.frame_id
		self.median_message.angle_min = laserscan.angle_min
		self.median_message.angle_max = laserscan.angle_max
		self.median_message.angle_increment = laserscan.angle_increment
		self.median_message.scan_time = laserscan.scan_time
		self.median_message.ranges = median_values
		self.median_message.range_min = min(median_values)
		self.median_message.range_max = max(median_values)
		self.median_message.intensities = in_array
		self.median_filter_pub.publish(self.median_message)


if __name__ == '__main__':
	rospy.init_node('mean_filter',log_level=rospy.DEBUG)
	filters = filters()
	rospy.spin()
